# prerequisite and install
```
: require python 3.6 to 3.8 from pyenv ref. bit.ly/pipenv

python -V; should_see='Python 3.8.3'
    python -m pip install --upgrade pip
    python -m pip install pipenv
        pipenv sync
```


# mongo database intro.ly
```
mongodb
    host:port/db  # db aka database
        db.collection.document

        term          dict key level
        ----------    --------------
        collection    root node
        document      under root node
                      a json object

        :db           can be thought of as a python dict object

        # collection   # document
        :db['key0']    = a_collection
                       = [ list-of-document ]

        :db['key'] = db['key-at_level-root'] = db['collection_name']

        :db['collection_name'] = [ list-of-document ]
        :db['collection_name'] = [
            document0,
            document1,
            document2,
            ...
        ]
            documentX = is_a_dict
            documentX = {
                'key1': {
                    'key2': ...
                }
            {
```
