import unittest
from datetime import datetime
from copy import deepcopy

import src.service.mongo as MongoSvc


MONGO_TEST_COLLECTION='my-pymongo-test'

INP = {
    'str'  : 'value00',
    'int'  : 122,
    'date' : datetime(2011, 12, 13),
}


class Test(unittest.TestCase):  # we need this :TestCase base class to hook the setUp() event

    def setUp(self):
        c=MongoSvc.connect(MONGO_TEST_COLLECTION); c.drop()  # c aka collection  # drop to ensure each testcase starts with fresh collection


    #region test insert
    def test_insert1(self):
        d = deepcopy(INP); d['_id'] = datetime.now()  # add this _id field to have unique ObjectId in mongo  # d aka document
        MongoSvc.insert(d, MONGO_TEST_COLLECTION)


    def test_insert2(self):
        d = deepcopy(INP); d['_id'] = datetime.now()  # add this _id field to have unique ObjectId in mongo  # d aka document
        c = MongoSvc.connect(MONGO_TEST_COLLECTION); c.insert_one(d)  # c aka collection

    #endregion test insert


    #region test query

    def test_query1_selectall(self):
        # create fixture
        c = MongoSvc.connect(MONGO_TEST_COLLECTION)  # c aka collection
        d=deepcopy(INP); c.insert_one(d)  # d aka document

        # testee code
        ri_all = c.find({})  # ri_all aka all_row_as_iterator

        # assert
        r_all = list(ri_all)  # convert to list
                              # r_all == :db['collection_name'] = [ list-of-document ]
                              # r_all == :db['collection_name'] = [ list-of-document ] - can be filtered with c.find({key:value})
        assert len(r_all)==1
        r = r_all[0]
        r.pop('_id')
        assert r == deepcopy(INP)


    def test_query2_withfilter(self):
        c = MongoSvc.connect(MONGO_TEST_COLLECTION)  # c aka collection

        # create fixture
        d1=deepcopy(INP); d1['_id']=1; c.insert_one(d1)  # dx aka document
        d2=deepcopy(INP); d2['_id']=2; c.insert_one(d2)

        # testee code
        ri=c.find({});        l=list(ri); assert len(l)==2  # ri_all aka all_row_as_iterator
        ri=c.find({'_id':1}); l=list(ri); assert len(l)==1; r=l[0]; assert r == d1  # l aka list
        ri=c.find({'_id':2}); l=list(ri); assert len(l)==1; r=l[0]; assert r == d2

    #endregion test query
