#!/usr/bin/env bash

echo '
#aftermath guide
After run, we can

    (from your machine) connect to mongo cli
    docker exec -it gc_mongo /bin/bash

    (when inside container)
    mongo

    here, you are ready to query mongo
Done

#sample mongo query
    show databases
    use :db
    show collections
'
