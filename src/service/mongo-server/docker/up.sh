#!/usr/bin/env bash
SH=`cd $(dirname $BASH_SOURCE)&& pwd`  # SH aka SCRIPT_HOME

#run the container
docker-compose -f "$SH/docker-compose.yml" up -d --force-recreate #ref. https://forums.docker.com/t/named-volume-with-postgresql-doesnt-keep-databases-data/7434/2
